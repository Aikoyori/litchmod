package net.litchschist.litchmod.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.inventory.EntityEquipmentSlot;


public class WaterBound extends Enchantment {

    protected WaterBound(Rarity rarityIn, EnumEnchantmentType typeIn, EntityEquipmentSlot[] slots) {
        super(rarityIn, EnumEnchantmentType.WEAPON, slots);
        this.setName("waterbound");
    }

    @Override
    public int getMinEnchantability(int enchantmentLevel) {
        // TODO Auto-generated method stub
        return 5 + (enchantmentLevel - 1) * 10;
    }

    @Override
    public int getMaxEnchantability(int enchantmentLevel) {

        return super.getMinEnchantability(enchantmentLevel) + 30;
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }
}
