package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PointArrow extends Item {
    public PointArrow() {
        setUnlocalizedName(NameReferences.MItems.POINTARROW.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.POINTARROW.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);
    }

}
