package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ElementZero extends Item {
    public ElementZero() {
        setUnlocalizedName(NameReferences.MItems.ELEMENTZERO.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.ELEMENTZERO.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }
}
