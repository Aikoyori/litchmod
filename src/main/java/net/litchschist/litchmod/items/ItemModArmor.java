package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

public class ItemModArmor extends ItemArmor {

    public ItemModArmor(ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn, String unlocName) {
        super(materialIn, renderIndexIn, equipmentSlotIn);
        this.setUnlocalizedName(unlocName);
        this.setRegistryName(unlocName);
        setCreativeTab(Mainclass.LitchArmorTab);
    }


}
