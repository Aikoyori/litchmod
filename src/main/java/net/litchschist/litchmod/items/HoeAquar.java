package net.litchschist.litchmod.items;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Multimap;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;

public class HoeAquar extends ItemHoe {
    private double attackDamage;
    private double attackSpeed;

    public HoeAquar(Item.ToolMaterial mat) {
        super(mat);
        this.attackDamage = 1.0D;
        this.attackSpeed = 1.0D;

        setUnlocalizedName(NameReferences.MItems.AQUARHOE.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.AQUARHOE.getRegistryName());
        setCreativeTab(Mainclass.LitchToolsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }

    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot) {
        Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);
        if (equipmentSlot == EntityEquipmentSlot.MAINHAND) {
            AttributeModifier atkDmgMod = new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", this.attackDamage, 0);
            AttributeModifier atkSpdMod = new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", this.attackSpeed, 0);
            multimap.remove(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.remove(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
        }
        return multimap;
    }



    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        RayTraceResult result = playerIn.rayTrace(10, 1F);
        Block bl = worldIn.getBlockState(result.getBlockPos()).getBlock();
        if(bl!= Blocks.FARMLAND&& bl!=Blocks.DIRT&& bl!=Blocks.GRASS)
        {
                Entity looking = getEntityMouseOver(1F, Minecraft.getMinecraft(), 10D);
                if (looking!=null)
                {
                    looking.posX=playerIn.posX;
                    looking.posY=playerIn.posY;
                    looking.posZ=playerIn.posZ;
                    looking.rotationYaw=playerIn.rotationYaw;
                    looking.rotationPitch=playerIn.rotationPitch;
                    looking.moveRelative(0F,0F,1F,0F);
                    return ActionResult.newResult(EnumActionResult.SUCCESS,this.getDefaultInstance());
                }

        }
        return ActionResult.newResult(EnumActionResult.FAIL,this.getDefaultInstance());
    }
    @Nullable
    public Entity getEntityMouseOver(float partialTicks,Minecraft mc,double rayTraceDistance) {
        Entity pointedEntity;
        Entity entity = mc.getRenderViewEntity();
        if (entity != null && mc.world != null) {
            mc.mcProfiler.startSection("pick");
            mc.pointedEntity = null;
            double d0 = rayTraceDistance;
            mc.objectMouseOver = entity.rayTrace(d0, partialTicks);
            Vec3d vec3d = entity.getPositionEyes(partialTicks);
            boolean flag = false;
            int i = 1;
            double d1 = d0;
            if (mc.playerController.extendedReach()) {
                d1 = 6.0D;
                d0 = d1;
            } else if (d0 > 3.0D) {
                flag = true;
            }

            if (mc.objectMouseOver != null) {
                d1 = mc.objectMouseOver.hitVec.distanceTo(vec3d);
            }

            Vec3d vec3d1 = entity.getLook(1.0F);
            Vec3d vec3d2 = vec3d.addVector(vec3d1.x * d0, vec3d1.y * d0, vec3d1.z * d0);
            pointedEntity = null;
            Vec3d vec3d3 = null;
            float f = 1.0F;
            List<Entity> list = mc.world.getEntitiesInAABBexcluding(entity, entity.getEntityBoundingBox().expand(vec3d1.x * d0, vec3d1.y * d0, vec3d1.z * d0).grow(1.0D, 1.0D, 1.0D), Predicates.and(EntitySelectors.NOT_SPECTATING, new Predicate<Entity>() {
                public boolean apply(@Nullable Entity p_apply_1_) {
                    return p_apply_1_ != null && p_apply_1_.canBeCollidedWith();
                }
            }));
            double d2 = d1;

            for(int j = 0; j < list.size(); ++j) {
                Entity entity1 = (Entity)list.get(j);
                AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().grow((double)entity1.getCollisionBorderSize());
                RayTraceResult raytraceresult = axisalignedbb.calculateIntercept(vec3d, vec3d2);
                if (axisalignedbb.contains(vec3d)) {
                    if (d2 >= 0.0D) {
                        pointedEntity = entity1;
                        vec3d3 = raytraceresult == null ? vec3d : raytraceresult.hitVec;
                        d2 = 0.0D;
                    }
                } else if (raytraceresult != null) {
                    double d3 = vec3d.distanceTo(raytraceresult.hitVec);
                    if (d3 < d2 || d2 == 0.0D) {
                        if (entity1.getLowestRidingEntity() == entity.getLowestRidingEntity() && !entity1.canRiderInteract()) {
                            if (d2 == 0.0D) {
                                pointedEntity = entity1;
                                vec3d3 = raytraceresult.hitVec;
                            }
                        } else {
                            pointedEntity = entity1;
                            vec3d3 = raytraceresult.hitVec;
                            d2 = d3;
                        }
                    }
                }
            }

            if (pointedEntity != null && flag && vec3d.distanceTo(vec3d3) > 3.0D) {
                pointedEntity = null;
                mc.objectMouseOver = new RayTraceResult(RayTraceResult.Type.MISS, vec3d3, (EnumFacing)null, new BlockPos(vec3d3));
            }

            if (pointedEntity != null && (d2 < d1 ||mc.objectMouseOver == null)) {
                mc.objectMouseOver = new RayTraceResult(pointedEntity, vec3d3);
                if (pointedEntity instanceof EntityLivingBase || pointedEntity instanceof EntityItemFrame) {
                    mc.pointedEntity = pointedEntity;
                }
            }


            return pointedEntity;
        }
        else
        {
            return null;
        }
    }
}
