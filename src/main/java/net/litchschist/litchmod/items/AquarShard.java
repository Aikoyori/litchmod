package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AquarShard extends Item {
    public AquarShard() {
        setUnlocalizedName(NameReferences.MItems.AQUARSHARD.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.AQUARSHARD.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }
}
