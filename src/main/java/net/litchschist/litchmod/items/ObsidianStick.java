package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;

public class ObsidianStick extends Item {
    public ObsidianStick() {

        setUnlocalizedName(NameReferences.MItems.OBSIDIANSTICK.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.OBSIDIANSTICK.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);

    }


}
