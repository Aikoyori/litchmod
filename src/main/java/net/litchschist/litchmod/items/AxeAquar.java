package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class AxeAquar extends ItemAxe {
    public AxeAquar(Item.ToolMaterial mat) {
        super(mat, 9.0F, -2.9F);
        setUnlocalizedName(NameReferences.MItems.AQUARAXE.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.AQUARAXE.getRegistryName());
        setCreativeTab(Mainclass.LitchToolsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }
}
