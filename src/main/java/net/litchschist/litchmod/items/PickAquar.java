package net.litchschist.litchmod.items;

import com.google.common.collect.Multimap;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class PickAquar extends ItemPickaxe {
    private double attackDamage;
    private double attackSpeed;

    public PickAquar(Item.ToolMaterial mat) {
        super(mat);
        this.attackDamage = 7.0D;
        this.attackSpeed = -2.5D;
        setUnlocalizedName(NameReferences.MItems.AQUARPICKAXE.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.AQUARPICKAXE.getRegistryName());
        setCreativeTab(Mainclass.LitchToolsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }

    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot) {
        Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);

        if (equipmentSlot == EntityEquipmentSlot.MAINHAND) {
            AttributeModifier atkDmgMod = new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", this.attackDamage, 0);
            AttributeModifier atkSpdMod = new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", this.attackSpeed, 0);
            multimap.remove(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.remove(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
        }

        return multimap;
    }

}
