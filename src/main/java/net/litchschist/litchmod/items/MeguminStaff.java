package net.litchschist.litchmod.items;

import com.google.common.collect.Multimap;
import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class MeguminStaff extends ItemSword {
    private double attackDamage;
    private double attackSpeed;
    private double move;

    public MeguminStaff() {
        super(ToolMaterial.IRON);
        this.attackDamage = 1.0D;
        this.attackSpeed = -3.0D;
        this.move=-0.5D;
        setUnlocalizedName(NameReferences.MItems.MEGUMINSTAFF.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.MEGUMINSTAFF.getRegistryName());
        setCreativeTab(Mainclass.LitchToolsTab);

    }



    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        {
            RayTraceResult result = playerIn.rayTrace(100, 1F);
            int x = result.getBlockPos().getX();
            int y = result.getBlockPos().getY();
            int z = result.getBlockPos().getZ();
            worldIn.createExplosion(playerIn, x, y,z , 30.0F,true );
            if(!playerIn.capabilities.isCreativeMode)
            {
                playerIn.getFoodStats().setFoodLevel(1);
                playerIn.setHealth(1);
                playerIn.addPotionEffect(new PotionEffect(Potion.getPotionById(2),1200,6,true,false));


            }
        }
        return super.onItemRightClick(worldIn,playerIn,handIn);
    }


    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot) {
        Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);
        if (equipmentSlot == EntityEquipmentSlot.MAINHAND) {
            AttributeModifier atkDmgMod = new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", this.attackDamage, 0);
            AttributeModifier atkSpdMod = new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", this.attackSpeed, 0);
            //AttributeModifier speed=new AttributeModifier(UUID.fromString("D3E12331-1122-2323-AAAA-123456789AB"),"Weapon Modifier",move,1);
            // multimap.remove(SharedMonsterAttributes.MOVEMENT_SPEED.getName(),speed);
            multimap.remove(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            multimap.remove(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            //multimap.put(SharedMonsterAttributes.MOVEMENT_SPEED.getName(),speed );
        }
        return multimap;
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving) {
        return super.onItemUseFinish(stack, worldIn, entityLiving);
    }

    protected void setBlock(ItemStack stack, EntityPlayer player, World worldIn, BlockPos pos, SoundEvent sound, IBlockState state)
    {
        worldIn.playSound(player, pos,sound, SoundCategory.BLOCKS, 1.0F, 1.0F);

        if (!worldIn.isRemote)
        {
            worldIn.setBlockState(pos, state, 11);
            stack.damageItem(1, player);
        }
    }
    protected boolean desBlock2(BlockPos pos,World worldIn, boolean dropBlock)
    {
        IBlockState iblockstate = worldIn.getBlockState(pos);
        Block block = iblockstate.getBlock();
        if (block.isAir(iblockstate, worldIn, pos))
        {
            return false;
        }
        else
        {
            worldIn.playEvent(2001, pos, Block.getStateId(iblockstate));

            if (dropBlock)
            {

            }

            return worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

}
