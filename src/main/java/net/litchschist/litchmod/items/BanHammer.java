package net.litchschist.litchmod.items;

import com.google.common.collect.Multimap;
import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class BanHammer extends ItemSword {
    private double attackDamage;
    private double attackSpeed;
    private double move;

    public BanHammer(ToolMaterial mat) {
        super(mat);
        this.attackDamage = 1336.0D;
        this.attackSpeed = -3.0D;
        this.move=-0.5D;
        setUnlocalizedName(NameReferences.MItems.BANHAMMER.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.BANHAMMER.getRegistryName());
        setCreativeTab(Mainclass.LitchToolsTab);

    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand hand) {
        {
            RayTraceResult result = playerIn.rayTrace(10000, 1F);
            int x = result.getBlockPos().getX();
            int y = result.getBlockPos().getY();
            int z = result.getBlockPos().getZ();

            EntityLightningBolt lightning = new EntityLightningBolt(worldIn, x, y, z, false);
            worldIn.addWeatherEffect(lightning);
        }
        return super.onItemRightClick(worldIn,playerIn,hand);
    }

    public Multimap<String, AttributeModifier> getItemAttributeModifiers(EntityEquipmentSlot equipmentSlot) {
        Multimap<String, AttributeModifier> multimap = super.getItemAttributeModifiers(equipmentSlot);
        if (equipmentSlot == EntityEquipmentSlot.MAINHAND) {
            AttributeModifier atkDmgMod = new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", this.attackDamage, 0);
            AttributeModifier atkSpdMod = new AttributeModifier(ATTACK_SPEED_MODIFIER, "Weapon modifier", this.attackSpeed, 0);
            //AttributeModifier speed=new AttributeModifier(UUID.fromString("D3E12331-1122-2323-AAAA-123456789AB"),"Weapon Modifier",move,1);
            // multimap.remove(SharedMonsterAttributes.MOVEMENT_SPEED.getName(),speed);
            multimap.remove(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            multimap.remove(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), atkDmgMod);
            multimap.put(SharedMonsterAttributes.ATTACK_SPEED.getName(), atkSpdMod);
            //multimap.put(SharedMonsterAttributes.MOVEMENT_SPEED.getName(),speed );
        }
        return multimap;
    }
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        {
            ItemStack itemstack = player.getHeldItem(hand);

            if (!player.canPlayerEdit(pos.offset(facing), facing, itemstack)) {
                return EnumActionResult.FAIL;
            } else {
                int hook = net.minecraftforge.event.ForgeEventFactory.onHoeUse(itemstack, player, worldIn, pos);
                if (hook != 0) return hook > 0 ? EnumActionResult.SUCCESS : EnumActionResult.FAIL;

                IBlockState iblockstate = worldIn.getBlockState(pos);
                Block block = iblockstate.getBlock();
                if (player.isSneaking()) {
                    if (block == Blocks.ANVIL) {

                        if ((iblockstate.getValue(BlockAnvil.DAMAGE)) == 2) {
                            worldIn.playEvent(2001, pos, Block.getStateId(iblockstate));
                            this.setBlock(itemstack, player, worldIn, pos, SoundEvents.BLOCK_ANVIL_DESTROY, Blocks.AIR.getDefaultState());
                            return EnumActionResult.SUCCESS;
                        } else {
                            this.setBlock(itemstack, player, worldIn, pos, SoundEvents.BLOCK_ANVIL_USE, Blocks.ANVIL.getDefaultState().withProperty(BlockAnvil.DAMAGE, (iblockstate.getValue(BlockAnvil.DAMAGE)) + 1).withProperty(BlockAnvil.FACING, iblockstate.getValue(BlockAnvil.FACING)));
                            worldIn.playSound(player, pos, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 1, 1);
                            return EnumActionResult.SUCCESS;
                        }
                    } else {
                        worldIn.destroyBlock(pos, true);
                        return EnumActionResult.SUCCESS;
                    }

                }
                else
                {
                    //worldIn.spawnEntity(new EntityLightningBolt(worldIn,hitX,hitY,hitZ,true));
                    worldIn.addWeatherEffect(new EntityLightningBolt(worldIn,pos.getX(),pos.getY(),pos.getZ(),false));
                    return EnumActionResult.SUCCESS;
                }

            }
        }
        //return EnumActionResult.PASS;
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving) {
        return super.onItemUseFinish(stack, worldIn, entityLiving);
    }

    protected void setBlock(ItemStack stack, EntityPlayer player, World worldIn, BlockPos pos, SoundEvent sound, IBlockState state)
    {
        worldIn.playSound(player, pos,sound, SoundCategory.BLOCKS, 1.0F, 1.0F);

        if (!worldIn.isRemote)
        {
            worldIn.setBlockState(pos, state, 11);
            stack.damageItem(1, player);
        }
    }
    protected boolean desBlock2(BlockPos pos,World worldIn, boolean dropBlock)
    {
        IBlockState iblockstate = worldIn.getBlockState(pos);
        Block block = iblockstate.getBlock();
        if (block.isAir(iblockstate, worldIn, pos))
        {
            return false;
        }
        else
        {
            worldIn.playEvent(2001, pos, Block.getStateId(iblockstate));

            if (dropBlock)
            {

            }

            return worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
        }
    }

}
