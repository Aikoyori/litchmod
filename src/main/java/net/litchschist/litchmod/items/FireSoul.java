package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class FireSoul extends Item {
    public FireSoul() {
        setUnlocalizedName(NameReferences.MItems.FIRESOUL.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.FIRESOUL.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }
}
