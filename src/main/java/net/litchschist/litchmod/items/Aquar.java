package net.litchschist.litchmod.items;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Aquar extends Item {
    public Aquar() {
        setUnlocalizedName(NameReferences.MItems.AQUAR.getUnlocalizedName());
        setRegistryName(NameReferences.MItems.AQUAR.getRegistryName());
        setCreativeTab(Mainclass.LitchItemsTab);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }
}
