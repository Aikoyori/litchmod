package net.litchschist.litchmod.init;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.blocks.AquarBlock;
import net.litchschist.litchmod.blocks.AquarLantern;
import net.litchschist.litchmod.blocks.AquarOre;
import net.litchschist.litchmod.blocks.AquarOreNether;
import net.litchschist.litchmod.blocks.AquarOreTheEnd;
import net.litchschist.litchmod.blocks.ecu.ElementConversionUnit;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.*;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.*;
import net.minecraftforge.registries.IForgeRegistry;

public class ModBlocks {
    /*----------------------------------------------Materials-------------------------------------------------*/



    public static void registerItemModel(Item item, int meta) {
        Mainclass.proxy.registerItemRenderer(item, meta, item.getUnlocalizedName());
    }

    public static Block blockAquar = new AquarBlock(Material.IRON);
    public static Block blockOreAquar = new AquarOre(Material.ROCK);
    public static Block blockOreNetherAquar = new AquarOreNether(Material.ROCK);
    public static Block blockOreTheEndAquar = new AquarOreTheEnd(Material.ROCK);
    public static Block blockAquarLantern = new AquarLantern(Material.GLASS);
    public static Block blockElementConversionUnit = new ElementConversionUnit(false);

    public static void modelRegisterer(Block block) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(block.getRegistryName(), "inventory"));
    }
    public static void register(IForgeRegistry<Block> registry) {
        registry.registerAll(blockAquar, blockOreAquar, blockOreNetherAquar, blockOreTheEndAquar, blockAquarLantern, blockElementConversionUnit);
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(createItemBlock(blockAquar), createItemBlock(blockOreAquar), createItemBlock(blockOreNetherAquar), createItemBlock(blockOreTheEndAquar), createItemBlock(blockAquarLantern), createItemBlock(blockElementConversionUnit));

    }

    public static void registerModels() {
        modelRegisterer(blockAquar);
        modelRegisterer(blockOreAquar);
        modelRegisterer(blockOreNetherAquar);
        modelRegisterer(blockOreTheEndAquar);
        modelRegisterer(blockAquarLantern);
        modelRegisterer(blockElementConversionUnit);
        registerItemModel(Item.getItemFromBlock(blockAquar), 0);
        registerItemModel(Item.getItemFromBlock(blockOreAquar), 0);
        registerItemModel(Item.getItemFromBlock(blockOreNetherAquar), 0);
        registerItemModel(Item.getItemFromBlock(blockOreTheEndAquar), 0);
        registerItemModel(Item.getItemFromBlock(blockAquarLantern), 0);
        registerItemModel(Item.getItemFromBlock(blockElementConversionUnit), 0);
    }

    public static Item createItemBlock(Block block) {
        return new ItemBlock(block).setRegistryName(block.getRegistryName());
    }
}
