package net.litchschist.litchmod.init;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.items.ItemModArmor;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.registries.IForgeRegistry;

public class ModArmor {
    public static ArmorMaterial aquarArmorMaterial = EnumHelper.addArmorMaterial("waterbound", Mainclass.modId + ":waterbound", 160, new int[]{6, 17, 12, 5}, 16, SoundEvents.BLOCK_WATERLILY_PLACE, 4.0f);
    public static ItemArmor aquarHelmet;
    public static ItemArmor aquarChestplate;
    public static ItemArmor aquarLeggings;
    public static ItemArmor aquarBoots;

    public static void init() {
        aquarHelmet = new ItemModArmor(aquarArmorMaterial, 1, EntityEquipmentSlot.HEAD, "waterbound_helmet");
        aquarChestplate = new ItemModArmor(aquarArmorMaterial, 1, EntityEquipmentSlot.CHEST, "waterbound_chestplate");
        aquarLeggings = new ItemModArmor(aquarArmorMaterial, 2, EntityEquipmentSlot.LEGS, "waterbound_leggings");
        aquarBoots = new ItemModArmor(aquarArmorMaterial, 1, EntityEquipmentSlot.FEET, "waterbound_boots");
    }

    public static void register(IForgeRegistry<Item> registry) {
        registry.register(aquarHelmet);
        registry.register(aquarChestplate);
        registry.register(aquarLeggings);
        registry.register(aquarBoots);
    }

    public static void registerModels() {
        registerItemModel(aquarHelmet);
        registerItemModel(aquarChestplate);
        registerItemModel(aquarLeggings);
        registerItemModel(aquarBoots);
    }

    public static void registerItemModel(Item item) {
        Mainclass.proxy.registerItemRenderer(item, 0, item.getUnlocalizedName());
    }

}
