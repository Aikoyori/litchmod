package net.litchschist.litchmod.init;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {
    public static void init() {
        GameRegistry.addSmelting(ModBlocks.blockOreAquar, new ItemStack(ModItems.aquarShard, 8), 15.0f);
        GameRegistry.addSmelting(ModBlocks.blockOreNetherAquar, new ItemStack(ModItems.aquarShard, 8), 15.0f);
        GameRegistry.addSmelting(ModBlocks.blockOreTheEndAquar, new ItemStack(ModItems.aquarShard, 8), 15.0f);
    }
}
