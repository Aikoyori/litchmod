package net.litchschist.litchmod.init;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.items.*;
import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;

public class ModItems {
    /*----------------------------------------------Materials-------------------------------------------------*/
    public static final Item.ToolMaterial materialAquar = EnumHelper.addToolMaterial("Waterbound", 4, 1920, 10, 4, 16);
    public static final Item.ToolMaterial heck = EnumHelper.addToolMaterial("opxd", 4, 2147483647, 9999, 99999, 66);

    public static void registerItemModel(Item item, int meta) {
        Mainclass.proxy.registerItemRenderer(item, meta, item.getUnlocalizedName());
    }

    public static Aquar aquar = new Aquar();
    public static Item firesoul = new FireSoul();
    public static Item elementZero = new ElementZero();
    public static AquarShard aquarShard = new AquarShard();
    public static Item obsidianStick = new ObsidianStick();
    public static PickAquar aquarPick = new PickAquar(materialAquar);
    public static Item aquarSword = new SwordAquar(materialAquar);
    public static Item aquarAxe = new AxeAquar(materialAquar);
    public static Item aquarShovel = new ShovelAquar(materialAquar);
    public static Item aquarHoe = new HoeAquar(materialAquar);
    public static Item banHammer = new BanHammer(heck);
    public static Item pointArrow = new PointArrow();
    public static Item meguminStaff = new MeguminStaff();
    public static void register(IForgeRegistry<Item> registry) {
        registry.registerAll(aquar, aquarShard, obsidianStick, aquarSword, aquarPick, aquarAxe, aquarShovel, aquarHoe, firesoul,banHammer,pointArrow,meguminStaff,elementZero);
    }

    public static void registerModels() {
        registerItemModel(aquar, 0);
        registerItemModel(aquarShard, 0);
        registerItemModel(obsidianStick, 0);
        registerItemModel(aquarPick, 0);
        registerItemModel(aquarAxe, 0);
        registerItemModel(aquarShovel, 0);
        registerItemModel(aquarSword, 0);
        registerItemModel(aquarHoe, 0);
        registerItemModel(firesoul, 0);
        registerItemModel(banHammer, 0);
        registerItemModel(pointArrow, 0);
        registerItemModel(meguminStaff, 0);
        registerItemModel(elementZero, 0);
    }

    public static void registerDict() {
        OreDictionary.registerOre("obsidianRod", obsidianStick);

    }
}
