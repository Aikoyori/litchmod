package net.litchschist.litchmod.world;

import java.util.Random;

import net.litchschist.litchmod.init.ModBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class ModWorldGeneration implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.getDimension()) {
            case 0:
                generateOverworld(random, chunkX, chunkZ, world, chunkGenerator, chunkProvider);
                break;
            case -1:
                generateNether(random, chunkX, chunkZ, world, chunkGenerator, chunkProvider);
                break;
            case 1:
                generateTheEnd(random, chunkX, chunkZ, world, chunkGenerator, chunkProvider);
                break;
        }
    }

    private void generateOverworld(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        generateOre(ModBlocks.blockOreAquar.getDefaultState(), BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX * 16, chunkZ * 16, 0, 16, 1 + random.nextInt(3), 3);
    }

    private void generateNether(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        generateOre(ModBlocks.blockOreNetherAquar.getDefaultState(), BlockMatcher.forBlock(Blocks.NETHERRACK), world, random, chunkX * 16, chunkZ * 16, 0, 255, 3 + random.nextInt(5), 10);
    }

    private void generateTheEnd(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        generateOre(ModBlocks.blockOreTheEndAquar.getDefaultState(), BlockMatcher.forBlock(Blocks.END_STONE), world, random, chunkX * 16, chunkZ * 16, 0, 90, 5 + random.nextInt(7), 6);
    }

    private void generateOre(IBlockState ore, BlockMatcher blockToReplace, World world, Random random, int x, int z, int minY, int maxY, int size, int chances) {
        int deltaY = maxY - minY;

        for (int i = 0; i < chances; i++) {
            BlockPos pos = new BlockPos(x + random.nextInt(16), minY + random.nextInt(deltaY), z + random.nextInt(16));

            WorldGenMinable generator = new WorldGenMinable(ore, size, blockToReplace);
            generator.generate(world, random, pos);
        }
    }
}
