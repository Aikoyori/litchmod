package net.litchschist.litchmod;

import java.rmi.registry.RegistryHandler;

import akka.Main;
import net.litchschist.litchmod.blocks.ecu.ElementConversionRecipes;
import net.litchschist.litchmod.blocks.ecu.TileEntityElementConversionUnit;
import net.litchschist.litchmod.enchantments.WaterBound;
import net.litchschist.litchmod.handler.GuiHandler;
import net.litchschist.litchmod.init.ModArmor;
import net.litchschist.litchmod.init.ModBlocks;
import net.litchschist.litchmod.init.ModItems;
import net.litchschist.litchmod.init.ModRecipes;
import net.litchschist.litchmod.proxy.CommonProxy;
import net.litchschist.litchmod.world.ModWorldGeneration;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;

import static net.minecraftforge.fml.common.Mod.*;

@Mod(modid = Mainclass.modId, name = Mainclass.name, version = Mainclass.version)
public class Mainclass {

    @SidedProxy(serverSide = "net.litchschist.litchmod.proxy.CommonProxy", clientSide = "net.litchschist.litchmod.proxy.ClientProxy")
    public static CommonProxy proxy;
    /*---------------------------------------------Creative Tabs----------------------------------------------*/
    public static final CreativeTabs LitchBlocksTab = new net.litchschist.litchmod.creativetabs.LitchBlocksTab();
    public static final CreativeTabs LitchItemsTab = new net.litchschist.litchmod.creativetabs.LitchItemsTab();
    public static final CreativeTabs LitchToolsTab = new net.litchschist.litchmod.creativetabs.LitchToolsTab();
    public static final CreativeTabs LitchArmorTab = new net.litchschist.litchmod.creativetabs.LitchArmorTab();
    /*-------------------------------------------Mod Information----------------------------------------------*/
    public static final String modId = "litchmod";
    public static final String name = "Litch Mod";
    public static final String version = "0.0.0-alpha+12";

    /*-------------------------------------------- OThers ----------------------------------------------------*/
    public static SimpleNetworkWrapper network;

    /*--------------------------------------EventBusSubscriber Thing------------------------------------------*/
    @Mod.EventBusSubscriber
    public static class RegistrationHandler {
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            ModArmor.init();
            ModItems.register(event.getRegistry());
            ModArmor.register(event.getRegistry());
            ModBlocks.registerItemBlocks(event.getRegistry());

        }

        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            ModBlocks.register(event.getRegistry());
        }

        @SubscribeEvent
        public static void registerModels(ModelRegistryEvent event) {
            System.err.println("THE MODEL IS GOING TO BE REGISTERED WTF");
            ModItems.registerModels();
            ModArmor.registerModels();
            ModBlocks.registerModels();
            System.err.println("I HAVE COMMITTED REGISTERINO");
        }
    }

    @Instance(modId)
    public static Mainclass instance;

    /*--------------------------------------------Pre/Init/Post-----------------------------------------------*/
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        GameRegistry.registerWorldGenerator(new ModWorldGeneration(), 2);
        GameRegistry.registerTileEntity(TileEntityElementConversionUnit.class, Mainclass.modId + ":TEElementCenversionUnit");

        System.err.println(name + " is loading!");
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(Mainclass.instance, new GuiHandler());
        ModItems.registerDict();
        ModRecipes.init();
        ElementConversionRecipes.getInstance();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
}
