package net.litchschist.litchmod;

public class NameReferences {
    /*----------------------------GUI------------------------------*/
    public static final int GUI_ELEMENT_CONVERTER = 666;

    /*---------------------------ITEMS-----------------------------*/
    public static enum MItems {
        POINTARROW("point_arrow_right"),
        AQUAR("waterbound_gem"),
        AQUARSHARD("waterbound_shard"),
        OBSIDIANSTICK("obsidian_stick"),
        AQUARSWORD("waterbound_sword"),
        AQUARSHOVEL("waterbound_shovel"),
        AQUARAXE("waterbound_axe"),
        AQUARHOE("waterbound_hoe"),
        AQUARPICKAXE("waterbound_pickaxe"),
        FIRESOUL("fire_soul"),
        ELEMENTZERO("element_zero"),
        BANHAMMER("banhammer"),
        MEGUMINSTAFF("megumin_staff");
        private String registryName;
        private String unlocalizedName;

        private MItems(String name) {
            this.unlocalizedName = this.registryName = name;
        }

        public String getUnlocalizedName() {
            return this.unlocalizedName;
        }

        public String getRegistryName() {
            return this.registryName;
        }
    }

    /*---------------------------BLOCKS-----------------------------*/
    public static enum MBlocks {
        AQUARBLOCK("waterbound_block"),
        AQUARORENETHER("waterbound_nether_ore"),
        AQUARORETHEEND("waterbound_end_ore"),
        AQUARORE("waterbound_overworld_ore"),
        AQUARLANTERN("waterbound_lantern"),
        ELEMENTCONVERSIONUNIT("element_conversion_unit");
        private String registryName;
        private String unlocalizedName;

        private MBlocks(String name) {
            this.unlocalizedName = this.registryName = name;
        }

        public String getUnlocalizedName() {
            return this.unlocalizedName;
        }

        public String getRegistryName() {
            return this.registryName;
        }
    }
}
