package net.litchschist.litchmod.utils;

public interface ICustomReach
{
    public float reach(); // default is 1.0D
}