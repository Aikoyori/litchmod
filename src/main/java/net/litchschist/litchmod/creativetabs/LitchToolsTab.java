package net.litchschist.litchmod.creativetabs;

import net.litchschist.litchmod.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class LitchToolsTab extends CreativeTabs {

    public LitchToolsTab() {
        super("litchtoolstab");
    }

    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(ModItems.aquarPick);
    }

}
