package net.litchschist.litchmod.creativetabs;

import net.litchschist.litchmod.init.ModBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class LitchBlocksTab extends CreativeTabs {

    public LitchBlocksTab() {
        super("litchblockstab");
    }

    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(ModBlocks.blockAquar);
    }

}
