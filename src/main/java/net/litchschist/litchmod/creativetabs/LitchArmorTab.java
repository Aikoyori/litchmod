package net.litchschist.litchmod.creativetabs;

import net.litchschist.litchmod.init.ModArmor;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class LitchArmorTab extends CreativeTabs {

    public LitchArmorTab() {
        super("litcharmorstab");
    }

    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(ModArmor.aquarHelmet);
    }

}
