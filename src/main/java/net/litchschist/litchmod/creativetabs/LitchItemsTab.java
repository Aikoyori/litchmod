package net.litchschist.litchmod.creativetabs;

import net.litchschist.litchmod.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class LitchItemsTab extends CreativeTabs {

    public LitchItemsTab() {
        super("litchitemstab");
    }

    @Override
    public ItemStack getTabIconItem() {
        return new ItemStack(ModItems.aquar);
    }

}
