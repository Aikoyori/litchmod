package net.litchschist.litchmod.handler;

import net.litchschist.litchmod.NameReferences;
import net.litchschist.litchmod.blocks.ecu.ContainerElementConversion;
import net.litchschist.litchmod.blocks.ecu.GuiElementConversionUnit;
import net.litchschist.litchmod.blocks.ecu.TileEntityElementConversionUnit;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == NameReferences.GUI_ELEMENT_CONVERTER) {
            return new ContainerElementConversion(player.inventory, (TileEntityElementConversionUnit) world.getTileEntity(new BlockPos(x, y, z)));

        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == NameReferences.GUI_ELEMENT_CONVERTER) {
            return new GuiElementConversionUnit(player.inventory, (TileEntityElementConversionUnit) world.getTileEntity(new BlockPos(x, y, z)));

        }
        return null;
    }

}
