package net.litchschist.litchmod.blocks;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;

public class AquarLantern extends Block {
    public AquarLantern(Material mat) {
        super(mat);
        setUnlocalizedName(NameReferences.MBlocks.AQUARLANTERN.getUnlocalizedName());
        setRegistryName(NameReferences.MBlocks.AQUARLANTERN.getRegistryName());
        setCreativeTab(Mainclass.LitchBlocksTab);
        setHardness(0.3F);
        setSoundType(SoundType.GLASS);
        setLightLevel(1.0F);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }

}
