package net.litchschist.litchmod.blocks;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AquarBlock extends Block {
    public AquarBlock(Material mat) {
        super(mat);
        setUnlocalizedName(NameReferences.MBlocks.AQUARBLOCK.getUnlocalizedName());
        setRegistryName(NameReferences.MBlocks.AQUARBLOCK.getRegistryName());
        setCreativeTab(Mainclass.LitchBlocksTab);
        setHardness(8.0F);
        setSoundType(SoundType.GLASS);
        setResistance(30.0F);
    }
    @Override
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.TRANSLUCENT;
    }
    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return true;
    }

    @Override
    public Block setLightOpacity(int opacity) {
        return super.setLightOpacity(5);
    }

    public boolean hasEffect(ItemStack itemstack) {
        return true;
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        super.getDrops(drops, world, pos, state, fortune);
    }
}
