package net.litchschist.litchmod.blocks;

import java.util.Random;

import net.litchschist.litchmod.Mainclass;
import net.litchschist.litchmod.NameReferences;
import net.litchschist.litchmod.init.ModItems;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class AquarOre extends Block {
    public AquarOre(Material mat) {
        super(mat);
        setUnlocalizedName(NameReferences.MBlocks.AQUARORE.getUnlocalizedName());
        setRegistryName(NameReferences.MBlocks.AQUARORE.getRegistryName());
        setHardness(3.0F);
        setResistance(5.0F);
        setHarvestLevel("pickaxe", 3);
        setCreativeTab(Mainclass.LitchBlocksTab);
    }

    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.aquarShard;
    }

    public int quantityDropped(Random random) {
        return 1 + random.nextInt(2);
    }

    public int quantityDroppedWithBonus(int fortune, Random random) {

        return this.quantityDropped(random) + fortune + 1;
    }

    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune) {
        Random rand = world instanceof World ? ((World) world).rand : new Random();
        if (this.getItemDropped(state, rand, fortune) != Item.getItemFromBlock(this)) {


            return MathHelper.getInt(rand, 30, 40);

        } else
            return MathHelper.getInt(rand, 30, 40);
    }

}
