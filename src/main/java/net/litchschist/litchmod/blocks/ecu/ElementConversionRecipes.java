package net.litchschist.litchmod.blocks.ecu;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;

import net.litchschist.litchmod.init.ModBlocks;
import net.litchschist.litchmod.init.ModItems;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class ElementConversionRecipes {
    private static final ElementConversionRecipes INSTANCE = new ElementConversionRecipes();
    private final Table<ItemStack, ItemStack, ItemStack> smeltingList = HashBasedTable.<ItemStack, ItemStack, ItemStack>create();
    private final Map<ItemStack, Float> experienceList = Maps.<ItemStack, Float>newHashMap();

    public static ElementConversionRecipes getInstance() {
        return INSTANCE;
    }

    public ElementConversionRecipes() {
        addConversionRecipe(new ItemStack(ModItems.aquar), new ItemStack(ModItems.aquar), new ItemStack(ModItems.firesoul), 20f,200);
        addConversionRecipe(new ItemStack(ModItems.firesoul), new ItemStack(ModItems.aquar), new ItemStack(ModItems.elementZero), 40f,200);
    }


    public void addConversionRecipe(ItemStack input1, ItemStack input2, ItemStack result, float experience,int fuelNeeded) {
        if (getConversionResult(input1, input2) != ItemStack.EMPTY) return;
        this.smeltingList.put(input1, input2, result);
        this.experienceList.put(result, Float.valueOf(experience));
    }

    public ItemStack getConversionResult(ItemStack input1, ItemStack input2) {
        for (Entry<ItemStack, Map<ItemStack, ItemStack>> entry : this.smeltingList.columnMap().entrySet()) {
            if (this.compareItemStacks(input1, (ItemStack) entry.getKey())) {
                for (Entry<ItemStack, ItemStack> ent : entry.getValue().entrySet()) {
                    if (this.compareItemStacks(input2, (ItemStack) ent.getKey())) {
                        return (ItemStack) ent.getValue();
                    }
                }
            }
        }
        return ItemStack.EMPTY;
    }

    private boolean compareItemStacks(ItemStack stack1, ItemStack stack2) {
        return stack2.getItem() == stack1.getItem() && (stack2.getMetadata() == 32767 || stack2.getMetadata() == stack1.getMetadata());
    }

    public Table<ItemStack, ItemStack, ItemStack> getDualSmeltingList() {
        return this.smeltingList;
    }

    public float getConversionExperience(ItemStack stack) {
        for (Entry<ItemStack, Float> entry : this.experienceList.entrySet()) {
            if (this.compareItemStacks(stack, (ItemStack) entry.getKey())) {
                return ((Float) entry.getValue()).floatValue();
            }
        }
        return 0.0F;
    }
}
