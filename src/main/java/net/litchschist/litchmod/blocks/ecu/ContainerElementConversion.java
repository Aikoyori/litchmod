package net.litchschist.litchmod.blocks.ecu;


import net.litchschist.litchmod.blocks.ecu.slot.SlotElementConversionUnitFuel;
import net.litchschist.litchmod.blocks.ecu.slot.SlotElementConversionUnitOut;
import net.litchschist.litchmod.blocks.ecu.slot.SlotElementConversionUnitReactant;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerElementConversion extends Container {
    private final TileEntityElementConversionUnit tileEnt;
    private int conversionTime, totalConversionTime, substanceAmount, totalSubstanceAmount;

    @Override

    public boolean canInteractWith(EntityPlayer playerIn) {

        return true;
    }

    public ContainerElementConversion(InventoryPlayer player, TileEntityElementConversionUnit tileentity) {
        this.tileEnt = tileentity;
        this.addSlotToContainer(new SlotElementConversionUnitReactant(tileentity, 0, 12, 35));
        this.addSlotToContainer(new SlotElementConversionUnitReactant(tileentity, 1, 32, 35));
        this.addSlotToContainer(new SlotElementConversionUnitFuel(tileentity, 2, 68, 35));
        this.addSlotToContainer(new SlotElementConversionUnitOut(player.player, tileentity, 3, 126, 35));
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                this.addSlotToContainer(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
            }
        }
        for (int x = 0; x < 9; x++) {
            this.addSlotToContainer(new Slot(player, x, x * 18 + 8, 142));
        }
    }

    @Override
    public void addListener(IContainerListener listener) {

        super.addListener(listener);
        listener.sendAllWindowProperties(this, this.tileEnt);
    }

    @Override
    public void detectAndSendChanges() {

        super.detectAndSendChanges();
        for (int i = 0; i < this.listeners.size(); ++i) {
            IContainerListener listener = (IContainerListener) this.listeners.get(i);
            if (this.conversionTime != this.tileEnt.getField(2))
                listener.sendWindowProperty(this, 2, tileEnt.getField(2));
            if (this.totalConversionTime != this.tileEnt.getField(3))
                listener.sendWindowProperty(this, 3, tileEnt.getField(3));
            if (this.substanceAmount != this.tileEnt.getField(0))
                listener.sendWindowProperty(this, 0, tileEnt.getField(0));
            if (this.totalSubstanceAmount != this.tileEnt.getField(1))
                listener.sendWindowProperty(this, 1, tileEnt.getField(1));
        }
        this.conversionTime = this.tileEnt.getField(2);
        this.totalConversionTime = this.tileEnt.getField(0);
        this.substanceAmount = this.tileEnt.getField(1);
        this.totalSubstanceAmount = this.tileEnt.getField(3);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int id, int data) {
        this.tileEnt.setField(id, data);
    }

    public boolean canInteractwith(EntityPlayer playerIn) {
        return this.tileEnt.isUsableByPlayer(playerIn);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = (Slot) this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack stack1 = slot.getStack();
            stack = stack1.copy();

            if (index == 3) {
                if (!this.mergeItemStack(stack1, 4, 40, true)) return ItemStack.EMPTY;
                slot.onSlotChange(stack1, stack);
            } else if (index != 2 && index != 1 && index != 0) {
                Slot slot1 = (Slot) this.inventorySlots.get(index);

                if (!ElementConversionRecipes.getInstance().getConversionResult(stack1, slot1.getStack()).isEmpty()) {
                    if (!this.mergeItemStack(stack1, 0, 2, false)) {
                        return ItemStack.EMPTY;
                    } else if (TileEntityElementConversionUnit.isConverter(stack1)) {
                        if (!this.mergeItemStack(stack1, 2, 3, false)) return ItemStack.EMPTY;
                    } else if (TileEntityElementConversionUnit.isConverter(stack1)) {
                        if (!this.mergeItemStack(stack1, 2, 3, false)) return ItemStack.EMPTY;
                    } else if (TileEntityElementConversionUnit.isConverter(stack1)) {
                        if (!this.mergeItemStack(stack1, 2, 3, false)) return ItemStack.EMPTY;
                    } else if (index >= 4 && index < 31) {
                        if (!this.mergeItemStack(stack1, 31, 40, false)) return ItemStack.EMPTY;
                    } else if (index >= 31 && index < 40 && !this.mergeItemStack(stack1, 4, 31, false)) {
                        return ItemStack.EMPTY;
                    }
                }
            } else if (!this.mergeItemStack(stack1, 4, 40, false)) {
                return ItemStack.EMPTY;
            }
            if (stack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();

            }
            if (stack1.getCount() == stack.getCount()) return ItemStack.EMPTY;
            slot.onTake(playerIn, stack1);
        }
        return stack;
    }
}