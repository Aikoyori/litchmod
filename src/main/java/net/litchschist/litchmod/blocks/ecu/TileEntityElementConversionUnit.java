package net.litchschist.litchmod.blocks.ecu;


import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;

public class TileEntityElementConversionUnit extends TileEntity implements IInventory, ITickable {
    private NonNullList<ItemStack> inventory = NonNullList.<ItemStack>withSize(4, ItemStack.EMPTY);
    private String customName;
    private int substanceAmount;
    private int currentSubstanceAmount;
    private int conversionTime;
    private int currentConversionTime;
    private boolean convertyes;
    @Override
    public String getName() {

        return this.hasCustomName() ? this.customName : "container.element_conversion_unit";
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return false;
    }

    @Override
    public boolean hasCustomName() {

        return this.customName != null && !this.customName.isEmpty();
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void clear() {
        this.inventory.clear();
    }

    public ITextComponent getDisplayName() {
        return this.hasCustomName() ? new TextComponentString(this.getName()) : new TextComponentTranslation(this.getName());
    }


    @Override
    public int getSizeInventory() {
        return this.inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack stack : this.inventory) {
            if (!stack.isEmpty()) return false;
        }
        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return (ItemStack) this.inventory.get(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return ItemStackHelper.getAndSplit(this.inventory, index, count);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.inventory, index);
    }

    public void setInventorySlotContents(int index, ItemStack stack) {
        ItemStack itemstack = this.inventory.get(index);
        boolean flag = !stack.isEmpty() && stack.isItemEqual(itemstack) && ItemStack.areItemStackTagsEqual(stack, itemstack);
        this.inventory.set(index, stack);

        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }

        if (index == 0 && !flag) {
            markDirty();
        }
        if (index == 2 && isSubstance(stack))
        {

            markDirty();

        }
    }

    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        this.inventory = NonNullList.<ItemStack>withSize(this.getSizeInventory(), ItemStack.EMPTY);
        ItemStackHelper.loadAllItems(compound, this.inventory);
        this.currentSubstanceAmount = compound.getInteger("TotalSubstanceAmount");
        this.conversionTime = compound.getInteger("ConversionTime");
        this.substanceAmount = compound.getInteger("SubstanceAmount");
        this.currentConversionTime = compound.getInteger("CurrentConversionTime");

        if (compound.hasKey("CustomName", 8)) {
            this.customName = compound.getString("CustomName");
        }
    }

    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setInteger("SubstanceAmount", (short) this.substanceAmount);
        compound.setInteger("ConversionTime", (short) this.conversionTime);
        compound.setInteger("TotalSubstanceAmount", (short) this.currentSubstanceAmount);
        compound.setInteger("CurrentConversionTime", (short) this.currentConversionTime);

        ItemStackHelper.saveAllItems(compound, this.inventory);

        if (this.hasCustomName()) {
            compound.setString("CustomName", this.customName);
        }

        return compound;
    }

    public boolean isConverting() {
        return this.currentConversionTime > 0 || convertyes;
    }
    public boolean hasFuel() {
        return this.currentSubstanceAmount > 1;
    }

    @SideOnly(Side.CLIENT)
    public static boolean isConverting(IInventory inventory) {
        return inventory.getField(0) > 0;
    }

    public void update() {
        boolean flag = this.isConverting();
        boolean isDirty = false;


        if (!this.world.isRemote) {
            ItemStack stack = (ItemStack) this.inventory.get(2);

            if (!((((ItemStack) this.inventory.get(0)).isEmpty()) || ((ItemStack) this.inventory.get(1)).isEmpty())) {
                if (!this.isConverting() && this.canConvert()&&!stack.isEmpty()) {
                    this.currentSubstanceAmount = getSubstanceAmount(stack);
                    this.substanceAmount = this.currentSubstanceAmount;
                    this.conversionTime = this.getConversionTime((ItemStack) this.inventory.get(0), (ItemStack) this.inventory.get(1));

                    stack.shrink(1);
                    ++currentConversionTime;

                    convertyes=true;
                    isDirty=true;
                }
                else if (this.isConverting()) {
                    if (this.currentSubstanceAmount > -1 && substanceAmount>0)
                    {
                        ++this.currentConversionTime;
                        --this.currentSubstanceAmount;
                        isDirty=true;
                    }
                    if (!stack.isEmpty() && this.currentSubstanceAmount <= -1)
                    {
                        this.currentSubstanceAmount=getSubstanceAmount(stack);
                        this.substanceAmount=getSubstanceAmount(stack);
                        stack.shrink(1);
                        isDirty = true;
                    }

                    if (this.conversionTime < this.currentConversionTime && isConverting()) {
                        this.currentConversionTime = 0;
                        this.conversionTime = 0;
                        this.convertItem();
                        convertyes=false;
                        isDirty = true;
                    }
                } else
                {
                    isDirty = true;
                }
            }
            if (flag != this.isConverting()) {
                isDirty = true;
            }
        }
        if (isDirty)
        {this.markDirty();
            ElementConversionUnit.setState(this.hasFuel(), this.world, this.pos);
        //System.err.println("Block is dirty at "+this.pos.toString()+" with ct cct sa csa: "+this.conversionTime+" "+this.currentConversionTime +" "+this.substanceAmount+" "+this.currentSubstanceAmount +" is converting "+this.isConverting()+" is convertible "+this.canConvert());
        }

    }

    private boolean canConvert() {
        if (((ItemStack) this.inventory.get(0)).isEmpty() || ((ItemStack) this.inventory.get(1)).isEmpty())
            return false;
        else {
            ItemStack result = ElementConversionRecipes.getInstance().getConversionResult((ItemStack) this.inventory.get(0), (ItemStack) this.inventory.get(1));
            if (result.isEmpty()) return false;
            else {
                ItemStack output = (ItemStack) this.inventory.get(3);
                if (output.isEmpty()) return true;
                if (!output.isItemEqual(result)) return false;
                int res = output.getCount() + result.getCount();
                return res <= getInventoryStackLimit() && res <= output.getMaxStackSize();
            }
        }
    }

    private static int getSubstanceAmount(ItemStack stack) {
        Item item = stack.getItem();
        if (stack.isEmpty()) {
            return 0;
        } else {
            if (item == (Items.BLAZE_POWDER)) {
                return 100;
            }
            if (item == (Items.COAL)) {
                return 10;
            } else return 0;
        }

    }

    public static boolean isConverter(ItemStack itemstack) {
        return getSubstanceAmount(itemstack) == 0 ? false : true;
    }

    private void convertItem() {
        if (this.canConvert()) {
            ItemStack input1 = (ItemStack) this.inventory.get(0);
            ItemStack input2 = (ItemStack) this.inventory.get(1);
            ItemStack result = ElementConversionRecipes.getInstance().getConversionResult(input1, input2);
            ItemStack output = (ItemStack) this.inventory.get(3);

            if (output.isEmpty()) this.inventory.set(3, result.copy());
            else if (output.getItem() == result.getItem()) output.grow(result.getCount());

            input1.shrink(1);
            input2.shrink(1);
        }

    }

    private int getConversionTime(ItemStack stack, ItemStack stack2) {
        // TODO Auto-generated method stub
        return 200;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        // TODO Auto-generated method stub
        return this.world.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;

    }

    @Override
    public void openInventory(EntityPlayer player) {
        // TODO Auto-generated method stub

    }

    @Override
    public void closeInventory(EntityPlayer player) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        // TODO Auto-generated method stub

        if (index == 3) return false;
        else if (index != 2) return true;
        else {
            return isSubstance(stack);
        }
    }

    public String getGuiID() {
        return "litchmod:element_conversion_unit";
    }

    public static boolean isSubstance(ItemStack stack) {
        return getSubstanceAmount(stack) > 0;
    }

    /**
     ** 0 : substance amount
     ** 1 : total
     ** 2 : conversion
     ** 3: current conversion
     */
    @Override
    public int getField(int id) {

        switch (id) {
            case 0:
                return this.substanceAmount;
            case 1:
                return this.currentSubstanceAmount;
            case 2:
                return this.conversionTime;
            case 3:
                return this.currentConversionTime;
            default:
                return 0;
        }
    }

    @Override
    public void setField(int id, int value) {
        switch (id) {
            case 0:
                this.substanceAmount = value;
                break;
            case 1:
                this.currentSubstanceAmount = value;
                break;
            case 2:
                this.conversionTime = value;
                break;
            case 3:
                this.currentConversionTime = value;
                break;

        }

    }

    @Override
    public int getFieldCount() {
        // TODO Auto-generated method stub
        return 4;
    }


}
