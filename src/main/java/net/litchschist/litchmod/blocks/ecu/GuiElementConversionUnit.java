package net.litchschist.litchmod.blocks.ecu;

import java.util.List;

import org.apache.commons.io.output.ThresholdingOutputStream;
import org.lwjgl.opengl.PixelFormat;

import net.litchschist.litchmod.Mainclass;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import scala.reflect.internal.Trees.Return;

public class GuiElementConversionUnit extends GuiContainer {
    int mouseX, mouseY;
    boolean debugUI = false;
    private static final ResourceLocation TEXTURES = new ResourceLocation(Mainclass.modId + ":textures/gui/element_conversion_unit.png");
    private final InventoryPlayer player;
    private final TileEntityElementConversionUnit tileentity;

    public GuiElementConversionUnit(InventoryPlayer player, TileEntityElementConversionUnit tileentity) {
        super(new ContainerElementConversion(player, tileentity));
        this.player = player;
        this.tileentity = tileentity;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {

        this.mouseX = mouseX * width / mc.displayWidth;
        this.mouseY = mouseY * height / mc.displayHeight - 1;
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseXX, int mouseYY) {
        String tileName = this.tileentity.getDisplayName().getFormattedText();
        this.fontRenderer.drawString(tileName, this.xSize / 2 - this.fontRenderer.getStringWidth(tileName) / 2, 8, 4210752);
        this.fontRenderer.drawString(this.player.getDisplayName().getFormattedText(), 8, this.ySize - 92, 4210752);

    }

    @Override
    public List<String> getItemToolTip(ItemStack stack) {
        return super.getItemToolTip(stack);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.drawDefaultBackground();
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);

        this.mc.getTextureManager().bindTexture(TEXTURES);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);

        if (TileEntityElementConversionUnit.isConverting(tileentity)) {
            int k = this.getConvertLeftScaled(14);
            this.drawTexturedModalRect(this.guiLeft + 51, this.guiTop + 36, 176, 0, k, 14);
        }
        int l = getConvertProgressScaled(24);
        this.drawTexturedModalRect(this.guiLeft + 89, this.guiTop + 35, 176, 14, l,17);
        if(debugUI)
        {
            this.fontRenderer.drawString("Substance we have : "+String.valueOf(this.tileentity.getField(0)),   this.guiLeft +89,  this.guiTop+220, 16777215);
            this.fontRenderer.drawString("Substance we currently have : "+String.valueOf(this.tileentity.getField(1)),   this.guiLeft +89,  this.guiTop+235, 16777215);
            this.fontRenderer.drawString("Time needed : "+String.valueOf(this.tileentity.getField(2)),   this.guiLeft +89,  this.guiTop+250, 16777215);
            this.fontRenderer.drawString("Time passed : "+String.valueOf(this.tileentity.getField(3)),   this.guiLeft +89,  this.guiTop+265, 16777215);
        }
}

    private int getConvertLeftScaled(int px) {
        int i = this.tileentity.getField(0);
        if (i == 0) i = 200;
        return this.tileentity.getField(1) * px / i;
    }

    private int getConvertProgressScaled(int px) {
        int i = this.tileentity.getField(3);
        int j = this.tileentity.getField(2);
        return j != 0 && i != 0 ? i * px / j : 0;
    }
}
