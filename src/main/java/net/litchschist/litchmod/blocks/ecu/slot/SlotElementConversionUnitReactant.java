package net.litchschist.litchmod.blocks.ecu.slot;

import net.litchschist.litchmod.blocks.ecu.TileEntityElementConversionUnit;
import net.litchschist.litchmod.init.ModItems;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotElementConversionUnitReactant extends Slot {

    public SlotElementConversionUnitReactant(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);

    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        if (stack.getItem() == ModItems.aquar)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return super.getItemStackLimit(stack);
    }
}
