package net.litchschist.litchmod.blocks.ecu.slot;

import net.litchschist.litchmod.blocks.ecu.TileEntityElementConversionUnit;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotElementConversionUnitFuel extends Slot {

    public SlotElementConversionUnitFuel(IInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);

    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return TileEntityElementConversionUnit.isSubstance(stack);
    }

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return super.getItemStackLimit(stack);
    }
}
