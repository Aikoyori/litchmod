package net.litchschist.litchmod.death;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;

import java.util.Iterator;
import java.util.List;

public class BanHammerDeath extends DamageSource {
    public BanHammerDeath(String damageTypeIn) {
        super(damageTypeIn);
    }
    public ITextComponent deathmsg = new ITextComponent() {
        @Override
        public ITextComponent setStyle(Style style) {
            return null;
        }

        @Override
        public Style getStyle() {
            return null;
        }

        @Override
        public ITextComponent appendText(String s) {
            return null;
        }

        @Override
        public ITextComponent appendSibling(ITextComponent iTextComponent) {
            return null;
        }

        @Override
        public String getUnformattedComponentText() {
            return null;
        }

        @Override
        public String getUnformattedText() {
            return null;
        }

        @Override
        public String getFormattedText() {
            return null;
        }

        @Override
        public List<ITextComponent> getSiblings() {
            return null;
        }

        @Override
        public ITextComponent createCopy() {
            return null;
        }

        @Override
        public Iterator<ITextComponent> iterator() {
            return null;
        }
    };
    public static DamageSource causeSwordDamage(Entity par0Entity, Entity par1Entity)
    {
        return (new EntityDamageSourceIndirect("sword", par0Entity, par1Entity));
    }


    @Override
    public ITextComponent getDeathMessage(EntityLivingBase entityLivingBaseIn) {
        return super.getDeathMessage(entityLivingBaseIn);
    }
}

